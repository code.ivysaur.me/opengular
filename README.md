# opengular

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A declarative two-way data binding library for Javascript.

The best way to determine if you understand something, is to reimplement it from memory.

## Features

- Angular.JS-style controller/scope/directive concepts, using hierarchal dirty scope checking
- Bind directive, supporting div/spans and two-way support for text inputs
- Click directive, with argument support loaded from current scope
- Repeat directive, with $index support and mutation protection
- Comes in at 300 lines (just 1.3KiB minified and gzipped) with no `eval()` tricks
- Permissive ISC license
- Depends on jQuery, but should work fine with API-compatibile substitutions like Zepto or jqLite

## Changelog

2015-10-18 1.0
- Initial public release
- [⬇️ opengular-1.0.tar.xz](dist-archive/opengular-1.0.tar.xz) *(30.71 KiB)*

